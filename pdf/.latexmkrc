# vim:ft=perl
$silent = 1;
$pdf_mode = 4;
$out_dir = "build";
$success_cmd = "pdftoppm -aa yes -aaVector yes -r 300 -png build/gender-terms.pdf -singlefile build/gender-terms";
@default_files = ("gender-terms.tex");
