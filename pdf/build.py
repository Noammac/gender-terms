#!/usr/bin/env python
# Build image and PDF using the specified terms
from configparser import ConfigParser
from itertools import zip_longest
from json import load
from os import mkdir, path
from subprocess import run
from dataclasses import dataclass
from typing import Iterable

config = ConfigParser()
config.read("terms.conf")


@dataclass
class Term:
    word: str
    categories: list[str]


def translate_term(dct) -> Term:
    return Term(dct['word'], [config['Terms'][cat] for cat in dct['categories']])


# List of all possible terms from which to choose and the catgories to which they belong.
terms_list: Iterable[Term]

with open("termslist.json", "r") as file:
    # Read the list of terms from the JSON config file.
    terms_list = load(file, object_hook=translate_term)

terms_list = filter(lambda term: ('remove' not in term.categories) and ('add' in term.categories), terms_list)
# Put selected terms in an ordered list.
terms = [fr"\checkbox{{{term.word}}}" for term in terms_list]

# Split the list of terms into three table columns.
length, remainder = divmod(len(terms), 3)
side_length = length + remainder // 2
left = terms[:side_length]
mid = terms[side_length:-side_length]
right = terms[-side_length:]

# Make a formatted table by taking the nth element of each column (or an empty string if such doesn't exist) and
# putting them in a LaTeX column configuration. Then take each "row" and add \\ at the end.
table = " \\\\\n".join(" &\n".join(triplet) for triplet in zip_longest(
    left,
    mid,
    right,
    fillvalue=""))

with open("table.tex", "w") as file:
    file.write(table)

if not path.exists("build"):
    mkdir("build")
run("latexmk")
