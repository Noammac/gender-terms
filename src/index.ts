import html2canvas from 'html2canvas';

type term = {
  word: string;
  categories: string;
  enabled: boolean;
};

type option = {
  name: string;
  text: string;
};

const root = document.querySelector<HTMLElement>(':root')!;
const container = document.getElementById('container')!;

const optionsElement = document.getElementById('options')!;
const colorControls = document.getElementById('color-controls')!;

const left = document.getElementById('left')!;
const center = document.getElementById('center')!;
const right = document.getElementById('right')!;

const terms: term[] = await fetch('termslist.json')
    .then((res) => res.json())
    .then((arr) =>
      arr.map((term: { word: string; categories: string[] }) => ({
        ...term,
        enabled: !term.categories.includes('slur'),
      })),
    ).catch(
        (_) => [{word: 'An error has occurred', categories: '', enabled: true}],
    );

const options: option[] = [
  {name: 'no', text: 'No'},
  {name: 'meh', text: 'Meh'},
  {name: 'okay', text: 'Okay'},
  {name: 'yes', text: 'Yes'},
  {name: 'slang', text: 'As slang'},
];
let selectedOption: string = '';

/**
 * Gets the value of a color variable.
 * @param {string} colorName: The name of the color variable.
 * @return {string} The value of the color variable
 */
function getColor(colorName: string): string {
  return getComputedStyle(root).getPropertyValue(`--${colorName}`);
}

/**
 * Sets the value of a color variable.
 * @param {string} colorName: The name of the color variable.
 * @param {string} colorValue: The value to set in the color variable.
 */
function setColor(colorName: string, colorValue: string): void {
  root.style.setProperty(`--${colorName}`, colorValue);
}

/**
 * Toggle the color circles like a group of radio buttons.
 * @param {HTMLButtonElement} swtch: The element to toggle.
 */
function toggleSwitch(swtch: HTMLButtonElement): void {
  let state: string;
  if (swtch.getAttribute('aria-checked')! === 'true') {
    state = 'false';
    selectedOption = '';
  } else {
    state = 'true';
    selectedOption = swtch.id.split('-')[1]!;
    document.querySelectorAll<HTMLButtonElement>('button.circ').forEach(
        (elem) => {
          elem.setAttribute('aria-checked', 'false');
          elem.value = 'false';
        },
    );
  }
  swtch.setAttribute('aria-checked', state);
  swtch.value = state;
}

/**
 * Add an option colour input.
 * @param {string} name: The name of the color to modify.
 * @param {string} text: The text with which to label the color input.
 */
function appendOptionInput(name: string, text: string): void {
  const colorInputLabel = document.createElement('label');
  const colorInput = document.createElement('input');
  colorInput.type = 'color';
  colorInput.autocomplete = 'off';
  colorInput.id = name;
  colorInput.name = name;
  colorInput.value = getColor(name);
  colorInput.addEventListener('change', (ev) => {
    setColor(name, (<HTMLInputElement> ev.target!).value);
  });

  colorInputLabel.appendChild(
      document.createTextNode(text),
  );
  colorInputLabel.appendChild(colorInput);
  colorControls.appendChild(colorInputLabel);
}

/**
 * Append an option to the options lists.
 * @param {option} opt: The option to append.
 */
function appendOption(opt: option) {
  appendOptionInput(opt.name, `Color of "${opt.text}"`);

  const colorOptContainer = document.createElement('div');
  const colorButton = document.createElement('button');
  colorButton.id = `circ-${opt.name}`;
  colorButton.name = 'options';
  colorButton.classList.add('shape', 'circ');
  colorButton.style.background = `var(--${opt.name})`;
  colorButton.setAttribute('role', 'radio');
  colorButton.setAttribute('aria-checked', 'false');
  colorButton.setAttribute('aria-labelledby', `label-${opt.name}`);
  colorButton.addEventListener('click', (ev) => {
    toggleSwitch(<HTMLButtonElement> ev.target!);
  });
  const colorLabel = document.createElement('label');
  colorLabel.contentEditable = 'true';
  colorLabel.id = `label-${opt.name}`;

  colorLabel.appendChild(document.createTextNode(opt.text));
  colorOptContainer.appendChild(colorButton);
  colorOptContainer.appendChild(colorLabel);
  optionsElement.appendChild(colorOptContainer);
}

/**
 * Toggle a term to the colour it is supposed to be.
 * Either run cyclically through the list, or set to the selected colour.
 * If there is a colour selected and the term isn't that colour, reset it.
 * @param {HTMLButtonElement} checkbox: The term checkbox whose colour to set.
 */
function toggleTerm(checkbox: HTMLButtonElement): void {
  let color: string;
  if (selectedOption !== '') {
    color = (checkbox.value === selectedOption ? '' : selectedOption);
  } else {
    const colors = options.map((opt) => opt.name).concat('');
    color = colors[(colors.indexOf(checkbox.value) + 1) % colors.length]!;
  }
  checkbox.style.background = (color ? `var(--${color})` : 'white');
  checkbox.value = color;
}

/**
 * Generates elements based on the list of terms and inserts them into
 * the term grid.
 */
function updateTerms(): void {
  // TODO: replace with Element.replaceChildren when possible
  left.innerText = '';
  center.innerText = '';
  right.innerText = '';

  const tags = terms.filter((term) => term.enabled).map((term) => {
    const lbl = document.createElement('label');
    const checkbox = document.createElement('button');

    checkbox.id = term.word.toLowerCase().replace(/\W/, '-');
    checkbox.classList.add('shape', 'checkbox');
    checkbox.addEventListener(
        'click',
        (ev) => toggleTerm(<HTMLButtonElement> ev.target!),
    );

    lbl.appendChild(checkbox);
    lbl.appendChild(document.createTextNode(term.word));
    return lbl;
  });

  const sideLength = (tags.length / 3) + Math.floor((tags.length % 3) / 2);
  tags.slice(0, sideLength).forEach((elem) => left.append(elem));
  tags.slice(sideLength, -sideLength).forEach((elem) => center.append(elem));
  tags.slice(-sideLength).forEach((elem) => right.append(elem));
}

/**
 * Capture the container element as a picture, give it a URL and display it.
 */
function capture(): void {
  const link = <HTMLLinkElement> document.getElementById('link')!;
  window.scroll(0, 0);
  container.style.setProperty('box-shadow', 'none');
  html2canvas(container).then((canvas) =>
    canvas.toBlob((blob) => {
      link.href = URL.createObjectURL(blob!);
      link.hidden = false;
    }),
  );
  container.style.removeProperty('box-shadow');
}

document.getElementById('capture')!.addEventListener('click', capture);
options.forEach(appendOption);
appendOptionInput('background', 'Background color');
updateTerms();

// vim:sw=2
